.. doctest docs/specs/products.rst
.. _specs.products:

===================================================
``products`` : defining the things you sell and buy
===================================================

.. currentmodule:: lino_xl.lib.products

The :mod:`lino_xl.lib.products` plugin adds functionality for managing
"products".

.. contents::
   :depth: 1
   :local:

.. include:: /../docs/shared/include/tested.rst

>>> from lino import startup
>>> startup('lino_pronto.projects.pronto1.settings.doctests')
>>> from lino.api.doctest import *



Products
========


.. class:: Products

    >>> rt.show(products.Products)
    ... #doctest: +ELLIPSIS +NORMALIZE_WHITESPACE +REPORT_UDIFF
    ==== ================================== ============================== ================================== ================================== ================================== ==================
     ID   Designation                        Designation (bn)               Designation (et)                   Designation (de)                   Designation (fr)                   Category
    ---- ---------------------------------- ------------------------------ ---------------------------------- ---------------------------------- ---------------------------------- ------------------
     1    Cable clip (size: 10mm)            তারের ক্লিপ (আকার: ১০মিমি)      Cable clip (size: 10mm)            Cable clip (size: 10mm)            Cable clip (size: 10mm)            Cable clip
     2    Cable clip (size: 14mm)            তারের ক্লিপ (আকার: ১৪মিমি)      Cable clip (size: 14mm)            Cable clip (size: 14mm)            Cable clip (size: 14mm)            Cable clip
     3    Cable clip (size: 20mm)            তারের ক্লিপ (আকার: ২০মিমি)      Cable clip (size: 20mm)            Cable clip (size: 20mm)            Cable clip (size: 20mm)            Cable clip
     4    Cable clip (size: 25mm)            তারের ক্লিপ (আকার: ২৫মিমি)      Cable clip (size: 25mm)            Cable clip (size: 25mm)            Cable clip (size: 25mm)            Cable clip
     5    Cable clip (size: 4mm)             তারের ক্লিপ (আকার: ৪মিমি)       Cable clip (size: 4mm)             Cable clip (size: 4mm)             Cable clip (size: 4mm)             Cable clip
     6    Cable clip (size: 5mm)             তারের ক্লিপ (আকার: ৫মিমি)       Cable clip (size: 5mm)             Cable clip (size: 5mm)             Cable clip (size: 5mm)             Cable clip
     7    Cable clip (size: 6mm)             তারের ক্লিপ (আকার: ৬মিমি)       Cable clip (size: 6mm)             Cable clip (size: 6mm)             Cable clip (size: 6mm)             Cable clip
     8    Cable clip (size: 7mm)             তারের ক্লিপ (আকার: ৭মিমি)       Cable clip (size: 7mm)             Cable clip (size: 7mm)             Cable clip (size: 7mm)             Cable clip
     9    Cable clip (size: 8mm)             তারের ক্লিপ (আকার: ৮মিমি)       Cable clip (size: 8mm)             Cable clip (size: 8mm)             Cable clip (size: 8mm)             Cable clip
     10   Ceiling holder                     সিলিং হোল্ডার                   Ceiling holder                     Ceiling holder                     Ceiling holder                     Holder
     11   Ceiling holder                     সিলিং হোল্ডার                   Ceiling holder                     Ceiling holder                     Ceiling holder                     Holder
     12   Distribution box (db box: 10-13)   বন্টন বাক্স (db box: ১০-১৩)      Distribution box (db box: 10-13)   Distribution box (db box: 10-13)   Distribution box (db box: 10-13)   Distribution box
     13   Distribution box (db box: 14-16)   বন্টন বাক্স (db box: ১৪-১৬)      Distribution box (db box: 14-16)   Distribution box (db box: 14-16)   Distribution box (db box: 14-16)   Distribution box
     14   Distribution box (db box: 4-6)     বন্টন বাক্স (db box: ৪-৬)        Distribution box (db box: 4-6)     Distribution box (db box: 4-6)     Distribution box (db box: 4-6)     Distribution box
     15   Distribution box (db box: 7-9)     বন্টন বাক্স (db box: ৭-৯)        Distribution box (db box: 7-9)     Distribution box (db box: 7-9)     Distribution box (db box: 7-9)     Distribution box
     16   Distribution box (db box: L1)      বন্টন বাক্স (db box: L1)         Distribution box (db box: L1)      Distribution box (db box: L1)      Distribution box (db box: L1)      Distribution box
     17   Distribution box (db box: L2)      বন্টন বাক্স (db box: L2)         Distribution box (db box: L2)      Distribution box (db box: L2)      Distribution box (db box: L2)      Distribution box
     18   Exhaust fan (size: 10 inches)      নিষ্কাশন পাখা (আকার: ১০ ইঞ্চি)   Exhaust fan (size: 10 inches)      Exhaust fan (size: 10 inches)      Exhaust fan (size: 10 inches)      Exhaust Fan
     19   Exhaust fan (size: 12 inches)      নিষ্কাশন পাখা (আকার: ১২ ইঞ্চি)   Exhaust fan (size: 12 inches)      Exhaust fan (size: 12 inches)      Exhaust fan (size: 12 inches)      Exhaust Fan
     20   Exhaust fan (size: 6 inches)       নিষ্কাশন পাখা (আকার: ৬ ইঞ্চি)    Exhaust fan (size: 6 inches)       Exhaust fan (size: 6 inches)       Exhaust fan (size: 6 inches)       Exhaust Fan
     21   Exhaust fan (size: 8 inches)       নিষ্কাশন পাখা (আকার: ৮ ইঞ্চি)    Exhaust fan (size: 8 inches)       Exhaust fan (size: 8 inches)       Exhaust fan (size: 8 inches)       Exhaust Fan
     22   Join box                           জয়েন বাক্স                      Join box                           Join box                           Join box                           Switch box
     23   Join box                           জয়েন বাক্স                      Join box                           Join box                           Join box                           Switch box
     28   PVC box (four gang)                PVC বাক্স (চার গ্যাং)            PVC box (four gang)                PVC box (four gang)                PVC box (four gang)                Switch box
     29   PVC box (one gang)                 PVC বাক্স (এক গ্যাং)             PVC box (one gang)                 PVC box (one gang)                 PVC box (one gang)                 Switch box
     30   PVC box (three gang)               PVC বাক্স (তিন গ্যাং)            PVC box (three gang)               PVC box (three gang)               PVC box (three gang)               Switch box
     31   PVC box (two gang)                 PVC বাক্স (দুই গ্যাং)            PVC box (two gang)                 PVC box (two gang)                 PVC box (two gang)                 Switch box
     24   Piano cutout                       পিয়ানো কাটআউট                  Piano cutout                       Piano cutout                       Piano cutout                       Piano cutout
     25   Piano indicator                    পিয়ানো ইন্ডিকেটর                Piano indicator                    Piano indicator                    Piano indicator                    Piano indicator
     26   Piano socket                       পিয়ানো সকেট                    Piano socket                       Piano socket                       Piano socket                       Piano socket
     27   Piano switch                       পিয়ানো সুইচ                    Piano switch                       Piano switch                       Piano switch                       Piano switch
     32   Steel box (four gang)              স্টিল বাক্স (চার গ্যাং)           Steel box (four gang)              Steel box (four gang)              Steel box (four gang)              Switch box
     33   Steel box (one gang)               স্টিল বাক্স (এক গ্যাং)            Steel box (one gang)               Steel box (one gang)               Steel box (one gang)               Switch box
     34   Steel box (three gang)             স্টিল বাক্স (তিন গ্যাং)           Steel box (three gang)             Steel box (three gang)             Steel box (three gang)             Switch box
     35   Steel box (two gang)               স্টিল বাক্স (দুই গ্যাং)           Steel box (two gang)               Steel box (two gang)               Steel box (two gang)               Switch box
     36   Tape                               টেপ                            Tape                               Tape                               Tape                               Wire tape
    ==== ================================== ============================== ================================== ================================== ================================== ==================
    <BLANKLINE>


.. class:: Services

    >>> rt.show(products.Services)
    ... #doctest: +ELLIPSIS +NORMALIZE_WHITESPACE -REPORT_UDIFF
    No data to display


.. class:: Category

    Can be used to group products into "categories".  Categories can be edited by the user.

    >>> rt.show(products.Categories)
    ... #doctest: +ELLIPSIS +NORMALIZE_WHITESPACE +REPORT_UDIFF
    ==== ============ ================== ================== ================== ================== ================== ==============
     ID   Parent       Designation        Designation (bn)   Designation (et)   Designation (de)   Designation (fr)   Product type
    ---- ------------ ------------------ ------------------ ------------------ ------------------ ------------------ --------------
     3                 Electrical         বৈদ্যুতিক           Electrical         Electrical         Electrical         Products
     4    Electrical   Fan                পাখা               Fan                Fan                Fan                Products
     5    Fan          Exhaust Fan        নিষ্কাশন পাখা       Exhaust Fan        Exhaust Fan        Exhaust Fan        Products
     6    Electrical   Cable clip         তারের ক্লিপ         Cable clip         Cable clip         Cable clip         Products
     7    Electrical   Distribution box   বন্টন বাক্স          Distribution box   Distribution box   Distribution box   Products
     8    Electrical   Wire tape          তারের টেপ          Wire tape          Wire tape          Wire tape          Products
     9    Electrical   Switch box         সুইচ বক্স           Switch box         Switch box         Switch box         Products
     10   Electrical   Switch             সুইচ               Switch             Switch             Switch             Products
     11   Switch       Piano switch       পিয়ানো সুইচ        Piano switch       Piano switch       Piano switch       Products
     12   Switch       Gang switch        গ্যাং সুইচ          Gang switch        Gang switch        Gang switch        Products
     13   Electrical   Socket             সকেট               Socket             Socket             Socket             Products
     14   Socket       Piano socket       পিয়ানো সকেট        Piano socket       Piano socket       Piano socket       Products
     15   Electrical   Cutout             কাটআউট             Cutout             Cutout             Cutout             Products
     16   Cutout       Piano cutout       পিয়ানো কাটআউট      Piano cutout       Piano cutout       Piano cutout       Products
     17   Electrical   Indicator          ইন্ডিকেটর           Indicator          Indicator          Indicator          Products
     18   Indicator    Piano indicator    পিয়ানো ইন্ডিকেটর    Piano indicator    Piano indicator    Piano indicator    Products
     19   Electrical   Holder             হোল্ডার             Holder             Holder             Holder             Products
     20   Electrical   Pipe               পাইপ               Pipe               Pipe               Pipe               Products
    ==== ============ ================== ================== ================== ================== ================== ==============
    <BLANKLINE>


.. class:: ProductTypes

    Can be used to group products into "types".  Types cannot be edited by the
    user.  But every product type can have a layout on its own.

    >>> rt.show(products.ProductTypes)
    ... #doctest: +ELLIPSIS +NORMALIZE_WHITESPACE -REPORT_UDIFF
    ======= ========== ========== ===================
     value   name       text       Table name
    ------- ---------- ---------- -------------------
     100     default    Products   products.Products
     200     services   Services   products.Services
     300     parts      Parts      products.Parts
    ======= ========== ========== ===================
    <BLANKLINE>


.. class:: DeliveryUnits

    The list of possible delivery units of a product.

    >>> rt.show(products.DeliveryUnits)
    ======= ======= ========
     value   name    text
    ------- ------- --------
     10      hour    Hours
     20      piece   Pieces
     30      kg      Kg
     40      box     Boxes
     45      pack    Packs
    ======= ======= ========
    <BLANKLINE>
