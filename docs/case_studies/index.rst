============================
Case studies for Lino Pronto
============================

.. toctree::
    :maxdepth: 1

    types_of_users

Current deployment status of prontobd
=====================================

At the moment :term:`prontobd` is available through the domain
https://pronto.8lurry.com to its single customer Rusel (a supplier).

Our current primary tasks is to create an index of all his products and have
them available to the sellers on search.

.. glossary::

    prontobd

        The nickname that we use to indicate the hosting/deployment of this
        project in Bangladesh.
