.. doctest docs/case_studies/types_of_users.rst
.. _pronto.case_studies.types_of_users:

=========
UserTypes
=========

.. currentmodule:: lino_pronto.lib.pronto

>>> from lino import startup
>>> startup('lino_pronto.projects.pronto1.settings.demo')
>>> from lino.api.doctest import *

.. class:: UserTypes

    See :class:`lino.modlib.users.choicelists.UserTypes`

In pronto we have the following :class:`UserTypes`::

    >>> rt.show("users.UserTypes")
    ======= ============== ===============
     value   name           text
    ------- -------------- ---------------
     000     anonymous      Anonymous
     100     user           User
     301     pos_agent      POS agent
     399     seller         Seller
     701     supply_staff   Supply staff
     799     supplier       Supplier
     800     manufacturer   Manufacturer
     900     admin          Administrator
    ======= ============== ===============
    <BLANKLINE>


Where:

- a `user` is just an ordinary person in the locality
- a `pos_agent` is a point of sale agent, employed in managing the sales of a shop
- a `seller` is a show owner in the locality
- a `supply_agent` is a person employed in managing the sales of a whole seller
- a `supplier` is a whole seller in the locality
- a `manufacturer` implicitly specifies the meaning
- an `admin` is a technical person how oversees the online pronto service
