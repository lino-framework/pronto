# -*- coding: utf-8 -*-
from atelier.sphinxconf import configure

configure(globals())
from lino.sphinxcontrib import configure

configure(globals(), 'lino_pronto.projects.pronto1.settings.demo')

extensions += ['lino.sphinxcontrib.logo']
project = "Lino Pronto website"
import datetime

copyright = '2012-{} Rumma & Ko Ltd'.format(datetime.date.today().year)

# html_context.update(public_url='https://pronto.lino-framework.org')
