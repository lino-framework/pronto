from atelier.invlib import setup_from_tasks

# docs are currently not maintained. To restart mataining them, uncomment the
# ``doc_trees=[]`` below. Last error message was "TypeError: '>=' not supported
# between instances of 'ImportError' and 'int'"

ns = setup_from_tasks(
    globals(),
    "lino_pronto",
    languages="en bn de fr et nl".split(),
    doc_trees=[],
    # tolerate_sphinx_warnings=True,
    blogref_url="https://luc.lino-framework.org",
    locale_dir='lino_pronto/lib/pronto/locale',
    revision_control_system='git',
    cleanable_files=['docs/api/lino_pronto.*'],
    demo_projects=['lino_pronto.projects.pronto1'])
