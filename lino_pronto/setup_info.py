# -*- coding: UTF-8 -*-
# Copyright 2014-2024 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)

SETUP_INFO = dict(name='lino-pronto',
                  version='19.1.0',
                  install_requires=['lino-xl', 'django-iban', 'lxml'],
                  tests_require=[
                      'beautifulsoup4',
                  ],
                  test_suite='tests',
                  description="A Lino for assembling and selling products",
                  long_description="""

**Lino Pronto** is a Lino application for assembling and selling products.

- Project homepage: https://gitlab.com/lino-framework/pronto

- Documentation: https://lino-framework.gitlab.io/pronto

- Commercial information: https://www.saffre-rumma.net

""",
                  author='Rumma & Ko Ltd',
                  author_email='info@lino-framework.org',
                  url="https://gitlab.com/lino-framework/pronto",
                  license_files=['COPYING'],
                  classifiers="""\
Programming Language :: Python
Programming Language :: Python :: 3
Development Status :: 1 - Planning
Environment :: Web Environment
Framework :: Django :: 3
Intended Audience :: Developers
Intended Audience :: System Administrators
License :: OSI Approved :: GNU Affero General Public License v3
Operating System :: OS Independent
Topic :: Office/Business :: Financial :: Accounting
""".splitlines())

SETUP_INFO.update(packages=[
    'lino_pronto',
    'lino_pronto.lib',
    'lino_pronto.lib.accounting',
    'lino_pronto.lib.accounting.fixtures',
    'lino_pronto.lib.contacts',
    'lino_pronto.lib.contacts.fixtures',
    'lino_pronto.lib.contacts.management',
    'lino_pronto.lib.contacts.management.commands',
    'lino_pronto.lib.invoicing',
    'lino_pronto.lib.products',
    'lino_pronto.lib.products.fixtures',
    'lino_pronto.lib.pronto',
    'lino_pronto.lib.shopping',
    'lino_pronto.lib.storage',
    'lino_pronto.lib.storage.fixtures',
    'lino_pronto.lib.users',
    'lino_pronto.lib.users.fixtures',
    'lino_pronto.lib.vat',
    'lino_pronto.lib.vat.fixtures',
    'lino_pronto.projects',
    'lino_pronto.projects.pronto1',
    'lino_pronto.projects.pronto1.settings',
    'lino_pronto.utils',
])

SETUP_INFO.update(
    # package_data=dict(),
    zip_safe=False,
    include_package_data=True)
