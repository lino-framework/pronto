# -*- coding: UTF-8 -*-
# Copyright 2024 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)
"""
"""

from lino.api import _
from lino_xl.lib.storage.models import *
from lino_xl.lib.storage.choicelists import ProvisionStates
from lino.core.actions import ShowTable

ProvisionStates.clear()
add = ProvisionStates.add_item

add('100', _("In stock"), 'in_stock')
add('200', _("Out of stock"), 'out_of_stock')
add('300', _("Ordered by customer"), 'ordered_cust')
add('400', _("Ordered from provider"), 'ordered_due')
add('500', _("Rented out"), 'rented_out')
add('600', _("Damaged"), 'damaged')
add('700', _("Under repair"), 'under_repair')

class ShowFillersTable(ShowTable):

    def get_action_permission(self, ar, obj, state):
        if ar.get_user().ledger is None:
            return False
        return super().get_action_permission(ar, obj, state)

class MyStorageFillers(FillersByPartner):

    @classmethod
    def get_default_action(cls):
        return ShowFillersTable()

    @classmethod
    def get_master_instance(self, ar, model, pk):
        return ar.get_user().ledger.company
