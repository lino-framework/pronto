# -*- coding: UTF-8 -*-
# Copyright 2024 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)
"""
"""

from lino_xl.lib.storage import Plugin


class Plugin(Plugin):
    needs_plugins = ['lino_pronto.lib.products', 'lino.modlib.summaries']

    def setup_quicklinks(self, tb):
        tb.add_action("storage.MyStorageFillers")
