from lino.api import rt
from lino.utils.instantiator import Instantiator
from lino.utils.cycler import Cycler
from random import randint


def objects():
    Company = rt.models.contacts.Company
    Product = rt.models.products.Product
    ProvisionStates = rt.models.storage.ProvisionStates
    filler_ = Instantiator("storage.Filler", "provision_product fill_asset min_asset").build

    ge_bd = Company.objects.get(barcode_identity=8)

    filler = lambda fill_asset, provision_state, product: filler_(product, str(fill_asset), "5", partner=ge_bd,
        provision_state=ProvisionStates.out_of_stock if fill_asset <= 5 and provision_state not in [
            ProvisionStates.out_of_stock, ProvisionStates.ordered_due] else provision_state)
            # ProvisionStates.out_of_stock, ProvisionStates.ordered_due] else ProvisionStates.in_stock)

    p_states = Cycler(ProvisionStates.choices)

    for p in (p_qs := Product.objects.all())[4:p_qs.count() - 4]:
        yield filler(randint(1, 100), p_states.pop()[0], p)
