# -*- coding: UTF-8 -*-
# Copyright 2024 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)

from lino.api import _
from lino_xl.lib.products.choicelists import *

DeliveryUnits.clear()
add = DeliveryUnits.add_item
add('10', _("Hours"), 'hour')
add('20', _("Pieces"), 'piece')
add('30', _("Kg"), 'kg')
add('40', _("Boxes"), 'box')
add('45', _("Packs"), 'pack')
