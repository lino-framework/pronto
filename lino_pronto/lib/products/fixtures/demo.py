# -*- coding: UTF-8 -*-
# Copyright 2024 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)

from lino.api import rt, dd, _
from lino.utils.instantiator import Instantiator
from lino_xl.lib.products.choicelists import ProductTypes, DeliveryUnits

def objects():
    productcat = Instantiator('products.Category').build
    product = Instantiator('products.Product', "vendor barcode_identity delivery_unit pieces_per_unit category").build
    partnerprice = Instantiator('products.PartnerPrice', "partner product trade_type price").build

    Company = rt.models.contacts.Company
    msm = Company.objects.get(barcode_identity=1)
    prodip = Company.objects.get(barcode_identity=2)
    oneplus = Company.objects.get(barcode_identity=3)
    osaca = Company.objects.get(barcode_identity=4)
    sk = Company.objects.get(barcode_identity=5)
    powerplus = Company.objects.get(barcode_identity=6)
    krs = Company.objects.get(barcode_identity=7)

    yield (electrical := productcat(id=3, product_type=ProductTypes.default, **dd.str2kw('name', _("Electrical"))))
    yield (fan := productcat(id=4, product_type=ProductTypes.default, parent=electrical, **dd.str2kw('name', _("Fan"))))
    yield (exhaustfan := productcat(
        id=5, product_type=ProductTypes.default, parent=fan, **dd.str2kw('name', _("Exhaust Fan"))))
    yield (cableclip := productcat(
        id=6, product_type=ProductTypes.default, parent=electrical, **dd.str2kw('name', _("Cable clip"))))
    yield (distributionbox := productcat(
        id=7, product_type=ProductTypes.default, parent=electrical, **dd.str2kw('name', _("Distribution box"))))
    yield (wiretape := productcat(
        id=8, product_type=ProductTypes.default, parent=electrical, **dd.str2kw('name', _("Wire tape"))))
    yield (switchbox := productcat(
        id=9, product_type=ProductTypes.default, parent=electrical, **dd.str2kw('name', _("Switch box"))))
    yield (switch := productcat(
        id=10, product_type=ProductTypes.default, parent=electrical, **dd.str2kw('name', _("Switch"))))
    yield (pianoswitch := productcat(
        id=11, product_type=ProductTypes.default, parent=switch, **dd.str2kw('name', _("Piano switch"))))
    yield (gangswitch := productcat(
        id=12, product_type=ProductTypes.default, parent=switch, **dd.str2kw('name', _("Gang switch"))))
    yield (socket := productcat(
        id=13, product_type=ProductTypes.default, parent=electrical, **dd.str2kw('name', _("Socket"))))
    yield (pianosocket := productcat(
        id=14, product_type=ProductTypes.default, parent=socket, **dd.str2kw('name', _("Piano socket"))))
    yield (cutout := productcat(
        id=15, product_type=ProductTypes.default, parent=electrical, **dd.str2kw('name', _("Cutout"))))
    yield (pianocutout := productcat(
        id=16, product_type=ProductTypes.default, parent=cutout, **dd.str2kw('name', _("Piano cutout"))))
    yield (indicator := productcat(
        id=17, product_type=ProductTypes.default, parent=electrical, **dd.str2kw('name', _("Indicator"))))
    yield (pianoindicator := productcat(
        id=18, product_type=ProductTypes.default, parent=indicator, **dd.str2kw('name', _("Piano indicator"))))
    yield (holder := productcat(
        id=19, product_type=ProductTypes.default, parent=electrical, **dd.str2kw('name', _("Holder"))))
    yield (pipe := productcat(
        id=20, product_type=ProductTypes.default, parent=electrical, **dd.str2kw('name', _("Pipe"))))

    yield (cableclip10 := product(
        msm, 1, DeliveryUnits.box, 100, cableclip, **dd.str2kw("name", _("Cable clip (size: 10mm)"))))

    from lino_xl.lib.accounting.choicelists import TradeTypes
    yield partnerprice(krs, cableclip10, TradeTypes.sales, 60)
    yield partnerprice(krs, cableclip10, TradeTypes.purchases, 52)
    yield (cableclip14 := product(
        msm, 2, DeliveryUnits.pack, 100, cableclip, **dd.str2kw("name", _("Cable clip (size: 14mm)"))))

    yield partnerprice(krs, cableclip14, TradeTypes.sales, 72)
    yield partnerprice(krs, cableclip14, TradeTypes.purchases, 60)
    yield (cableclip20 := product(
        msm, 3, DeliveryUnits.pack, 100, cableclip, **dd.str2kw("name", _("Cable clip (size: 20mm)"))))

    yield partnerprice(krs, cableclip20, TradeTypes.sales, 130)
    yield partnerprice(krs, cableclip20, TradeTypes.purchases, 115)
    yield (cableclip25 := product(
        msm, 4, DeliveryUnits.pack, 100, cableclip, **dd.str2kw("name", _("Cable clip (size: 25mm)"))))

    yield partnerprice(krs, cableclip25, TradeTypes.sales, 180)
    yield partnerprice(krs, cableclip25, TradeTypes.purchases, 165)
    yield (cableclip4 := product(
        msm, 5, DeliveryUnits.box, 100, cableclip, **dd.str2kw("name", _("Cable clip (size: 4mm)"))))

    yield partnerprice(krs, cableclip4, TradeTypes.sales, 25)
    yield partnerprice(krs, cableclip4, TradeTypes.purchases, 20)
    yield (cableclip5 := product(
        msm, 6, DeliveryUnits.box, 100, cableclip, **dd.str2kw("name", _("Cable clip (size: 5mm)"))))

    yield partnerprice(krs, cableclip5, TradeTypes.sales, 26)
    yield partnerprice(krs, cableclip5, TradeTypes.purchases, 22)
    yield (cableclip6 := product(
        msm, 7, DeliveryUnits.pack, 100, cableclip, **dd.str2kw("name", _("Cable clip (size: 6mm)"))))

    yield partnerprice(krs, cableclip6, TradeTypes.sales, 30)
    yield partnerprice(krs, cableclip6, TradeTypes.purchases, 26)
    yield (cableclip7 := product(
        msm, 8, DeliveryUnits.box, 100, cableclip, **dd.str2kw("name", _("Cable clip (size: 7mm)"))))

    yield partnerprice(krs, cableclip7, TradeTypes.sales, 35)
    yield partnerprice(krs, cableclip7, TradeTypes.purchases, 30)
    yield (cableclip8 := product(
        msm, 9, DeliveryUnits.box, 100, cableclip, **dd.str2kw("name", _("Cable clip (size: 8mm)"))))
    # TODO: replace dd.babel_values with dd.str2kw

    yield partnerprice(krs, cableclip8, TradeTypes.sales, 45)
    yield partnerprice(krs, cableclip8, TradeTypes.purchases, 38)
    yield (ceilingholder1p := product(
        oneplus, 1, DeliveryUnits.piece, None, holder, **dd.str2kw("name", _("Ceiling holder"))))

    yield partnerprice(krs, ceilingholder1p, TradeTypes.sales, 75)
    yield partnerprice(krs, ceilingholder1p, TradeTypes.purchases, 72)
    yield (ceilingholdermsm := product(
        msm, 10, DeliveryUnits.piece, None, holder, **dd.str2kw("name", _("Ceiling holder"))))

    yield partnerprice(krs, ceilingholdermsm, TradeTypes.sales, 32)
    yield partnerprice(krs, ceilingholdermsm, TradeTypes.purchases, 29)
    yield (distributionbox10_13 := product(prodip, 1, DeliveryUnits.piece, None, distributionbox, **dd.str2kw(
            "name", _("Distribution box (db box: 10-13)"))))

    yield partnerprice(krs, distributionbox10_13, TradeTypes.sales, 520)
    yield partnerprice(krs, distributionbox10_13, TradeTypes.purchases, 500)
    yield (distributionbox14_16 := product(oneplus, 2, DeliveryUnits.piece, None, distributionbox, **dd.str2kw(
            "name", _("Distribution box (db box: 14-16)"))))

    yield partnerprice(krs, distributionbox14_16, TradeTypes.sales, 750)
    yield partnerprice(krs, distributionbox14_16, TradeTypes.purchases, 700)
    yield (distributionbox4_6 := product(oneplus, 3, DeliveryUnits.piece, None, distributionbox, **dd.str2kw(
            "name", _("Distribution box (db box: 4-6)"))))

    yield partnerprice(krs, distributionbox4_6, TradeTypes.sales, 340)
    yield partnerprice(krs, distributionbox4_6, TradeTypes.purchases, 360)
    yield (distributionbox7_9 := product(oneplus, 4, DeliveryUnits.piece, None, distributionbox, **dd.str2kw(
            "name", _("Distribution box (db box: 7-9)"))))

    yield partnerprice(krs, distributionbox7_9, TradeTypes.sales, 400)
    yield partnerprice(krs, distributionbox7_9, TradeTypes.purchases, 370)
    yield (distributionboxL1 := product(powerplus, 1, DeliveryUnits.piece, None, distributionbox, **dd.str2kw(
            "name", _("Distribution box (db box: L1)"))))

    yield partnerprice(krs, distributionboxL1, TradeTypes.sales, 26)
    yield partnerprice(krs, distributionboxL1, TradeTypes.purchases, 23)
    yield (distributionboxL2 := product(
        msm, 11, DeliveryUnits.piece, None, distributionbox, **dd.str2kw("name", _("Distribution box (db box: L2)"))))

    yield partnerprice(krs, distributionboxL2, TradeTypes.sales, 30)
    yield partnerprice(krs, distributionboxL2, TradeTypes.purchases, 26)
    yield (exhaustfan10 := product(
        oneplus, 5, DeliveryUnits.piece, None, exhaustfan, **dd.str2kw("name", _("Exhaust fan (size: 10 inches)"))))

    yield partnerprice(krs, exhaustfan10, TradeTypes.sales, 710)
    yield partnerprice(krs, exhaustfan10, TradeTypes.purchases, 675)
    yield (exhaustfan12 := product(
        oneplus, 6, DeliveryUnits.piece, None, exhaustfan, **dd.str2kw("name", _("Exhaust fan (size: 12 inches)"))))

    yield partnerprice(krs, exhaustfan12, TradeTypes.sales, 820)
    yield partnerprice(krs, exhaustfan12, TradeTypes.purchases,790)
    yield (exhaustfan6 := product(
        oneplus, 7, DeliveryUnits.piece, None, exhaustfan, **dd.str2kw("name", _("Exhaust fan (size: 6 inches)"))))

    yield partnerprice(krs, exhaustfan6, TradeTypes.sales, 620)
    yield partnerprice(krs, exhaustfan6, TradeTypes.purchases, 580)
    yield (exhaustfan8 := product(
        oneplus, 8, DeliveryUnits.piece, None, exhaustfan, **dd.str2kw("name", _("Exhaust fan (size: 8 inches)"))))

    yield partnerprice(krs, exhaustfan8, TradeTypes.sales, 670)
    yield partnerprice(krs, exhaustfan8, TradeTypes.purchases, 605)
    yield (joinbox1p := product(
        oneplus, 9, DeliveryUnits.piece, None, switchbox, **dd.str2kw("name", _("Join box"))))

    yield partnerprice(krs, joinbox1p, TradeTypes.sales, 33)
    yield partnerprice(krs, joinbox1p, TradeTypes.purchases, 31.5)
    yield (joinboxmsm := product(
        msm, 12, DeliveryUnits.piece, None, switchbox, **dd.str2kw("name", _("Join box"))))

    yield partnerprice(krs, joinboxmsm, TradeTypes.sales, 25)
    yield partnerprice(krs, joinboxmsm, TradeTypes.purchases, 23)
    yield (pianocutoutmsm := product(
        msm, 13, DeliveryUnits.pack, 12, pianocutout, **dd.str2kw("name", _("Piano cutout"))))

    yield partnerprice(krs, pianocutoutmsm, TradeTypes.sales, 150)
    yield partnerprice(krs, pianocutoutmsm, TradeTypes.purchases, 130)
    yield (pianoindicatormsm := product(
        msm, 14, DeliveryUnits.piece, None, pianoindicator, **dd.str2kw("name", _("Piano indicator"))))

    yield partnerprice(krs, pianoindicatormsm, TradeTypes.sales, 35)
    yield partnerprice(krs, pianoindicatormsm, TradeTypes.purchases, 30)
    yield (pianosocketmsm := product(
        msm, 15, DeliveryUnits.pack, 12, pianosocket, **dd.str2kw("name", _("Piano socket"))))

    yield partnerprice(krs, pianosocketmsm, TradeTypes.sales, 150)
    yield partnerprice(krs, pianosocketmsm, TradeTypes.purchases, 130)
    yield (pianoswitchmsm := product(
        msm, 16, DeliveryUnits.pack, 12, pianoswitch, **dd.str2kw("name", _("Piano switch"))))

    yield partnerprice(krs, pianoswitchmsm, TradeTypes.sales, 150)
    yield partnerprice(krs, pianoswitchmsm, TradeTypes.purchases, 130)
    yield (pvcbox4g := product(
        oneplus, 10, DeliveryUnits.piece, None, switchbox, **dd.str2kw("name", _("PVC box (four gang)"))))

    yield partnerprice(krs, pvcbox4g, TradeTypes.sales, 52)
    yield partnerprice(krs, pvcbox4g, TradeTypes.purchases, 48)
    yield (pvcbox1g := product(
        oneplus, 11, DeliveryUnits.piece, None, switchbox, **dd.str2kw("name", _("PVC box (one gang)"))))

    yield partnerprice(krs, pvcbox1g, TradeTypes.sales, 14)
    yield partnerprice(krs, pvcbox1g, TradeTypes.purchases, 12)
    yield (pvcbox3g := product(
        oneplus, 12, DeliveryUnits.piece, None, switchbox, **dd.str2kw("name", _("PVC box (three gang)"))))

    yield partnerprice(krs, pvcbox3g, TradeTypes.sales, 45)
    yield partnerprice(krs, pvcbox3g, TradeTypes.purchases, 40)
    yield (pvcbox2g := product(
        oneplus, 13, DeliveryUnits.piece, None, switchbox, **dd.str2kw("name", _("PVC box (two gang)"))))

    yield partnerprice(krs, pvcbox2g, TradeTypes.sales, 28)
    yield partnerprice(krs, pvcbox2g, TradeTypes.purchases, 25)
    yield (steelbox4g := product(
        sk, 1, DeliveryUnits.piece, None, switchbox, **dd.str2kw("name", _("Steel box (four gang)"))))

    yield partnerprice(krs, steelbox4g, TradeTypes.sales, 105)
    yield partnerprice(krs, steelbox4g, TradeTypes.purchases, 100)
    yield (steelbox1g := product(
        sk, 2, DeliveryUnits.piece, None, switchbox, **dd.str2kw("name", _("Steel box (one gang)"))))

    yield partnerprice(krs, steelbox1g, TradeTypes.sales, 30)
    yield partnerprice(krs, steelbox1g, TradeTypes.purchases, 29)
    yield (steelbox3g := product(
        sk, 3, DeliveryUnits.piece, None, switchbox, **dd.str2kw("name", _("Steel box (three gang)"))))

    yield partnerprice(krs, steelbox3g, TradeTypes.sales, 85)
    yield partnerprice(krs, steelbox3g, TradeTypes.purchases, 79)
    yield (steelbox2g := product(
        sk, 4, DeliveryUnits.piece, None, switchbox, **dd.str2kw("name", _("Steel box (two gang)"))))

    yield partnerprice(krs, steelbox2g, TradeTypes.sales, 62)
    yield partnerprice(krs, steelbox2g, TradeTypes.purchases, 59)
    yield (tape := product(osaca, 1, DeliveryUnits.piece, None, wiretape, **dd.str2kw("name", _("Tape"))))

    yield partnerprice(krs, tape, TradeTypes.sales, 165)
    yield partnerprice(krs, tape, TradeTypes.purchases, 150)
