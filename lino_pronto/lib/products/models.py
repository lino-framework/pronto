# -*- coding: UTF-8 -*-
# Copyright 2017-2024 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)

from django.contrib.contenttypes.fields import GenericRelation
from lino_xl.lib.products.models import *
from lino.api import _
from lino.core import constants
from lino.modlib.uploads.actions import CameraStream
from lino.modlib.uploads.roles import UploadsReader
from lino_xl.lib.accounting.choicelists import TradeTypes
from lino_xl.lib.vat.choicelists import VatClasses
from .choicelists import *

ProductTypes.clear()
add = ProductTypes.add_item
add('100', _("Products"), 'default', table_name="products.Products")
add('200', _("Services"), 'services', table_name="products.Services")
add('300', _("Parts"), 'parts', table_name="products.Parts")
# add('300', _("Other"), 'default')


class CameraStream(CameraStream):
    action_name = 'capture_product_image'
    select_rows = True

    def run_from_ui(self, ar, **kwargs):
        kwargs.update({"owner": ar.selected_rows[0]})
        kwargs.update(**ar.action_param_values)
        self.handle_uploaded_file(ar, **kwargs)
        ar.success(refresh=True)


class ImagesByProduct(dd.Table):
    label = _("Pictures")
    model = "uploads.Upload"
    required_roles = dd.login_required(UploadsReader)
    master_key = "owner"
    display_mode = ((None, constants.DISPLAY_MODE_GALLERY), )


class PartnerPrice(dd.Model):
    class Meta:
        app_label = 'products'
        verbose_name = _("Partner price")
        verbose_name_plural = _("Partner prices")
        unique_together = ('partner', 'product', 'trade_type')

    partner = dd.ForeignKey('contacts.Company')
    product = dd.ForeignKey('products.Product')
    trade_type = TradeTypes.field(default=TradeTypes.sales)
    price = dd.DecimalField(max_digits=32, decimal_places=2)


class Product(Product):
    class Meta(Product.Meta):
        unique_together = ('vendor', 'barcode_identity')

    camera_stream = CameraStream()
    images = GenericRelation('uploads.Upload',
                             content_type_field='owner_type',
                             object_id_field='owner_id',
                             related_query_name='product')

    @dd.virtualfield(dd.DecimalField(verbose_name=_("Sales price"), max_digits=32, decimal_places=2, null=True, blank=True), editable=True)
    def my_sales_price(self, ar=None):
        if ar is None or (user := ar.get_user()).is_anonymous or (c := user.ledger.company) is None:
            return None
        PartnerPrice = rt.models.products.PartnerPrice
        try:
            pp = PartnerPrice.objects.get(product=self, partner=c, trade_type=TradeTypes.sales)
        except PartnerPrice.DoesNotExist:
            if 'my_sales_price' not in ar.rqdata:
                return None
            pp = None
        if 'my_sales_price' in ar.rqdata:
            if pp is None:
                pp = PartnerPrice(product=self, partner=c, trade_type=TradeTypes.sales)
            pp.price = ar.rqdata['my_sales_price']
            pp.full_clean()
            pp.save()
        return pp.price

    @dd.virtualfield(dd.DecimalField(verbose_name=_('Purchases price'), max_digits=32, decimal_places=2, null=True, blank=True), editable=True)
    def my_purchases_price(self, ar):
        if ar is None or (user := ar.get_user()).is_anonymous or (c := user.ledger.company) is None:
            return None
        PartnerPrice = rt.models.products.PartnerPrice
        try:
            pp = PartnerPrice.objects.get(product=self, partner=c, trade_type=TradeTypes.purchases)
        except PartnerPrice.DoesNotExist:
            if 'my_purchases_price' not in ar.rqdata:
                return None
            pp = None
        if 'my_purchases_price' in ar.rqdata:
            if pp is None:
                pp = PartnerPrice(product=self, partner=c, trade_type=TradeTypes.purchases)
            pp.price = ar.rqdata['my_purchases_price']
            pp.full_clean()
            pp.save()
        return pp.price

    def disabled_fields(self, ar):
        df = super().disabled_fields(ar)
        if (user := ar.get_user()).is_anonymous or user.ledger is None:
            df.add('my_purchases_price')
            df.add('my_sales_price')
        return df


dd.update_field('products.Product', 'vat_class', default=VatClasses.vatless)


class ProductDetail(dd.DetailLayout):

    main = "general sales storage.FillersByProduct"

    general = dd.Panel(
        """
    name
    #barcode id product_type category delivery_unit barcode_svg
    body
    products.ImagesByProduct
    """, _("General"))

    sales = dd.Panel(
        """
    my_purchases_price my_sales_price vat_class
    trading.InvoiceItemsByProduct
    """, _("Sales"))


# Products.column_names = "name tariff sales_price sales_account *"


class ProductCard(dd.DetailLayout):
    main = """
    product_image:30 card_content:70
    """

    card_content = """
    name
    category sales_price
    body_short_preview
    """


class Products(Products):
    column_names = "id name category *"
    card_layout = ProductCard()

    @dd.displayfield(_("Image"))
    def product_image(self, obj, ar):
        img = obj.images.first()
        if img is None:
            return ""
        return '<img src="{}" style="height: 15ch; max-width: 22.5ch">'.format(
            img.get_file_url())
        # return '<img src="{}" style="max-height: 100%; width: 20ch;">'.format(img.get_file_url())


class ProductsQuickInsert(Products):
    column_names = "name vendor category body delivery_unit pieces_per_unit vat_class my_sales_price my_purchases_price product_image *"
    stay_in_grid = True
    insert_layout = None

    @classmethod
    def get_actor_label(cls):
        return "Products quick insert"


class Categories(Categories):
    stay_in_grid = True
    # insert_layout = """
    # name
    # body
    # parent
    # """


class Services(Products):
    _product_type = ProductTypes.services
    column_names = "name sales_account *"


class Parts(Products):
    _product_type = ProductTypes.parts
    column_names = "name sales_account *"


# from lino.core.roles import UserRole
#
# class Nobody(UserRole):
#     pass
#
# Categories.required_roles = dd.login_required(Nobody)
