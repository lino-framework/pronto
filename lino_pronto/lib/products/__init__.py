# -*- coding: UTF-8 -*-
# Copyright 2017-2024 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)
"""
The :ref:`pronto` extension of :mod:`lino_xl.lib.products`.

"""

from lino_xl.lib.products import Plugin, _


class Plugin(Plugin):
    extends_models = ['Product']
    barcode_driver = "ean13"

    def setup_config_menu(self, site, user_type, m, ar=None):
        super().setup_config_menu(site, user_type, m, ar)
        mg = self.get_menu_group()
        m = m.add_menu(mg.app_label, mg.verbose_name)
        m.add_action('products.ProductsQuickInsert')
