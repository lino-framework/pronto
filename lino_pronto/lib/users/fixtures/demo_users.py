from lino.modlib.users.fixtures.demo_users import *

other_objects = objects


def objects():
    for obj in other_objects():
        yield obj

    User = settings.SITE.user_model
    yield User(first_name='Russel', last_name='Ibrahim', username='russel',
               user_type=UserTypes.supplier, email=settings.SITE.demo_email)
    yield User(first_name='Cleo', last_name='Patra', username='cleopatra',
               user_type=UserTypes.seller, email=settings.SITE.demo_email)
    yield User(first_name='Abu', last_name='Nomani', username='abunomani',
               user_type=UserTypes.pos_agent, email=settings.SITE.demo_email)
    yield User(first_name='Huntress', last_name='Gretel', username='gretel',
               user_type=UserTypes.supply_staff, email=settings.SITE.demo_email)
