# -*- coding: UTF-8 -*-
# Copyright 2024 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)
"""
Django model (db tables) definitions for :mod:`lino_pronto.lib.shopping` plugin.
"""

from lino.api import _
from lino_xl.lib.shopping.models import *
from lino.modlib.users.mixins import StartPlan
from .roles import CartUser


class StartPlan(StartPlan):
    required_roles = dd.login_required(CartUser)
    def get_button_label(self, actor):
        return _("New shopping cart")


class Cart(Cart):

    manager_roles_required = dd.login_required(CartUser)
    # workflow_owner_field = "partner"

    class Meta(Cart.Meta):
        abstract = dd.is_abstract_model(__name__, 'Cart')

    partner = dd.ForeignKey('contacts.Partner', null=True, blank=True, on_delete=models.SET_DEFAULT,
                            default=lambda : rt.models.contacts.Company.objects.get(name="Miscellaneous"))

    start_new_plan = StartPlan()

Carts.detail_layout = """user partner today delivery_method
    invoicing_address delivery_address invoice
    shopping.ItemsByCart
    """


class CartForPartner(Carts):
    hide_navigator = True
    required_roles = dd.login_required(CartUser)
