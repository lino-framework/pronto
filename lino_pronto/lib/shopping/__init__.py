# -*- coding: UTF-8 -*-
# Copyright 2024 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)
"""Shopping plugin for a :ref:`pronto` project. This
inherits from :mod:`lino_xl.lib.shopping`.

"""

from lino_xl.lib.shopping import Plugin


class Plugin(Plugin):

    extends_models = ["Cart"]

    def get_quicklinks(self):
        yield 'shopping.CartForPartner.start_new_plan'
