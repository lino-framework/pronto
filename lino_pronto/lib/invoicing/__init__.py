# -*- coding: UTF-8 -*-
# Copyright 2024 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)
"""This is Lino pronto's standard plugin for General invoicing.
Inherits from :mod:`lino_xl.lib.invoicing` See :doc:`/plugins/invoicing`.

"""

from lino_xl.lib.invoicing import Plugin
