# -*- coding: UTF-8 -*-
# Copyright 2024 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)
"""This is Lino pronto's standard plugin for General accounting.
Inherits from :mod:`lino_xl.lib.accounting` See :doc:`/plugins/accounting`.

"""

from lino_xl.lib.accounting import Plugin
