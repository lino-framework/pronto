from lino.api import rt, dd, _

def objects():
    PaymentMethod = rt.models.accounting.PaymentMethod

    def payment_method(designation, **kwargs):
        return PaymentMethod(
            **dd.str2kw('designation', designation, **kwargs))

    yield payment_method(_("Cash payment"), is_cash=True)
    # yield payment_method(_("PayPal"))
    yield payment_method(_("bKash"))
