# -*- coding: UTF-8 -*-
# Copyright 2015-2024 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)
"""Defines a default set of user types and fills
:class:`lino.modlib.users.choicelists.UserTypes`.

"""

from lino.api import _
from lino.modlib.users.choicelists import UserTypes
from lino.core.roles import UserRole, SiteAdmin, SiteUser
from lino.modlib.office.roles import OfficeStaff, OfficeUser
from lino.modlib.uploads.roles import UploadsReader
from lino_xl.lib.excerpts.roles import ExcerptsUser, ExcerptsStaff
from lino_xl.lib.contacts.roles import ContactsUser, ContactsStaff
from lino_xl.lib.products.roles import ProductsUser, ProductsStaff
from lino_xl.lib.storage.roles import StorageUser, StorageStaff
from lino_xl.lib.accounting.roles import LedgerUser, LedgerStaff
from lino_xl.lib.sepa.roles import SepaUser, SepaStaff
from lino_xl.lib.courses.roles import CoursesUser
from lino_pronto.lib.shopping.roles import CartUser, CartStaff


class SiteUser(SiteUser, CartUser, CoursesUser, ContactsUser, OfficeUser, LedgerUser, SepaUser,
               ExcerptsUser, UploadsReader):
    pass


class POSAgent(SiteUser, ContactsStaff, StorageUser):
    """Point of sale agent"""


class Seller(SiteUser, CartStaff, ProductsUser, StorageUser, ContactsStaff, LedgerStaff):
    """Shop owners"""


class SupplyStaff(SiteUser, ContactsStaff, StorageUser):
    """Some frontend user who looks after the CUD records within a Supplier's scope."""


class Supplier(SiteUser, ProductsStaff, StorageStaff, ContactsStaff, LedgerStaff):
    """Whole sellers"""


class Manufacturer(SiteUser):
    pass


class SiteAdmin(SiteAdmin, ContactsStaff, OfficeStaff, CoursesUser,
                LedgerStaff, SepaStaff, ExcerptsStaff, ProductsStaff,
                StorageStaff):
    pass


UserTypes.clear()

add = UserTypes.add_item

add('000', _("Anonymous"), UserRole, name='anonymous', readonly=True)
add('100', _("User"), SiteUser, name='user')
add('301', _("POS agent"), POSAgent, name='pos_agent')
add('399', _("Seller"), Seller, name='seller')
add('701', _("Supply staff"), Supplier, name='supply_staff')
add('799', _("Supplier"), SupplyStaff, name='supplier')
add('800', _("Manufacturer"), Manufacturer, name='manufacturer')
add('900', _("Administrator"), SiteAdmin, name='admin')
