��    1      �  C   ,      8  
   9     D     \     t     �     �     �     �     �                &     -      >      _     �     �     �     �  
   �               /     M     j     �     �     �  	   �     �     �     �     �     �                     *     7     <     C     Y     n     �     �  
   �     �  	   �  j  �     &	  B   F	  B   �	  B   �	  B   
  ?   R
  ?   �
  ?   �
  ?     ?   R  %   �     �     �  7   �  7   #  1   [  1   �  ,   �  ,   �       %   5  L   [  L   �  I   �  I   ?     �     �     �     �     �  /     ,   2  /   _  /   �  %   �  .   �       "   4     W     d  ;   q  8   �  ;   �  ;   "     ^     k  	   �     �            &                                 (                                     %   /      *          +                       )      -   '   0                     ,          .   "   
                 #          !   	      $          1    Cable clip Cable clip (size: 10mm) Cable clip (size: 14mm) Cable clip (size: 20mm) Cable clip (size: 25mm) Cable clip (size: 4mm) Cable clip (size: 5mm) Cable clip (size: 6mm) Cable clip (size: 7mm) Cable clip (size: 8mm) Ceiling holder Cutout Distribution box Distribution box (db box: 10-13) Distribution box (db box: 14-16) Distribution box (db box: 4-6) Distribution box (db box: 7-9) Distribution box (db box: L1) Distribution box (db box: L2) Electrical Exhaust Fan Exhaust fan (size: 10 inches) Exhaust fan (size: 12 inches) Exhaust fan (size: 6 inches) Exhaust fan (size: 8 inches) Fan Gang switch Holder Indicator Join box PVC box (four gang) PVC box (one gang) PVC box (three gang) PVC box (two gang) Piano cutout Piano indicator Piano socket Piano switch Pipe Socket Steel box (four gang) Steel box (one gang) Steel box (three gang) Steel box (two gang) Switch Switch box Tape Wire tape Project-Id-Version: lino-pronto 19.1.0
Report-Msgid-Bugs-To: EMAIL@ADDRESS
PO-Revision-Date: 2024-02-26 20:50+0600
Last-Translator: 
Language-Team: bn <LL@li.org>
Language: bn
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
Generated-By: Babel 2.11.0
X-Generator: Poedit 3.0.1
 তারের ক্লিপ তারের ক্লিপ (আকার: ১০মিমি) তারের ক্লিপ (আকার: ১৪মিমি) তারের ক্লিপ (আকার: ২০মিমি) তারের ক্লিপ (আকার: ২৫মিমি) তারের ক্লিপ (আকার: ৪মিমি) তারের ক্লিপ (আকার: ৫মিমি) তারের ক্লিপ (আকার: ৬মিমি) তারের ক্লিপ (আকার: ৭মিমি) তারের ক্লিপ (আকার: ৮মিমি) সিলিং হোল্ডার কাটআউট বন্টন বাক্স বন্টন বাক্স (db box: ১০-১৩) বন্টন বাক্স (db box: ১৪-১৬) বন্টন বাক্স (db box: ৪-৬) বন্টন বাক্স (db box: ৭-৯) বন্টন বাক্স (db box: L1) বন্টন বাক্স (db box: L2) বৈদ্যুতিক নিষ্কাশন পাখা নিষ্কাশন পাখা (আকার: ১০ ইঞ্চি) নিষ্কাশন পাখা (আকার: ১২ ইঞ্চি) নিষ্কাশন পাখা (আকার: ৬ ইঞ্চি) নিষ্কাশন পাখা (আকার: ৮ ইঞ্চি) পাখা গ্যাং সুইচ হোল্ডার ইন্ডিকেটর জয়েন বাক্স PVC বাক্স (চার গ্যাং) PVC বাক্স (এক গ্যাং) PVC বাক্স (তিন গ্যাং) PVC বাক্স (দুই গ্যাং) পিয়ানো কাটআউট পিয়ানো ইন্ডিকেটর পিয়ানো সকেট পিয়ানো সুইচ পাইপ সকেট স্টিল বাক্স (চার গ্যাং) স্টিল বাক্স (এক গ্যাং) স্টিল বাক্স (তিন গ্যাং) স্টিল বাক্স (দুই গ্যাং) সুইচ সুইচ বক্স টেপ তারের টেপ 