# -*- coding: UTF-8 -*-
# Copyright 2011-2024 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)
"""Default settings module for a :ref:`pronto` project. This is being
inherited by the demo projects in :mod:`lino_pronto.projects`.

"""

from lino_pronto import SETUP_INFO
from lino.projects.std.settings import *


class Site(Site):
    """Base class for a :ref:`pronto` application.

    """

    verbose_name = "Lino Pronto"
    description = SETUP_INFO['description']
    version = SETUP_INFO['version']
    url = SETUP_INFO['url']

    demo_fixtures = 'std few_countries minimal_ledger demo demo2'.split()

    # languages = 'en de fr'
    languages = 'en'

    user_types_module = 'lino_pronto.lib.pronto.user_types'
    custom_layouts_module = 'lino_pronto.lib.pronto.layouts'

    default_build_method = 'weasy2pdf'

    # textfield_format = 'html'

    def get_plugin_configs(self):
        yield super().get_plugin_configs()
        yield 'users', 'allow_online_registration', True
        yield 'accounting', 'has_purchases', True
        yield 'accounting', 'has_payment_methods', True
        # yield 'vat', 'declaration_plugin', 'lino_xl.lib.bdvat'

    def get_installed_plugins(self):
        yield super(Site, self).get_installed_plugins()
        yield 'lino.modlib.gfks'
        # yield 'lino.modlib.system'
        yield 'lino_pronto.lib.users'
        yield 'lino_xl.lib.countries'
        yield 'lino_pronto.lib.contacts'
        #~ yield 'lino_xl.lib.households'

        yield 'lino_xl.lib.excerpts'

        # yield 'lino_xl.lib.outbox'
        yield 'lino.modlib.uploads'
        yield 'lino.modlib.weasyprint'
        yield 'lino.modlib.export_excel'
        yield 'lino.modlib.tinymce'
        # yield 'lino.modlib.wkhtmltopdf'

        # accounting must come before trading because its demo fixture
        # creates journals (?)

        yield 'lino_pronto.lib.products'
        yield 'lino_pronto.lib.accounting'
        yield 'lino_xl.lib.ledgers'
        yield 'lino_xl.lib.sepa'
        yield 'lino_xl.lib.finan'
        yield 'lino_pronto.lib.vat'
        # yield 'lino_xl.lib.vat'
        yield 'lino_xl.lib.bdvat'
        # yield 'lino_xl.lib.bevat'
        yield 'lino_xl.lib.trading'
        yield 'lino_pronto.lib.invoicing'
        yield 'lino_pronto.lib.storage'
        yield 'lino_pronto.lib.shopping'
        #~ 'lino.modlib.journals',
        #~ 'lino_xl.lib.projects',
        #~ yield 'lino_xl.lib.blogs'
        #~ yield 'lino.modlib.tickets'
        #~ 'lino.modlib.links',
        #~ 'lino_xl.lib.thirds',
        #~ yield 'lino_xl.lib.postings'
        # yield 'lino_xl.lib.pages'
        yield 'lino_pronto.lib.pronto'

    def setup_plugins(self):
        """
        Change the default value of certain plugin settings.

        """
        super(Site, self).setup_plugins()
        self.plugins.countries.configure(hide_region=True)
        self.plugins.accounting.configure(use_pcmn=True)
        self.plugins.countries.configure(country_code='BE')
        # self.plugins.products.configure(menu_group='sales')


USE_TZ = True
TIME_ZONE = 'UTC'
