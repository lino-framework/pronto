# Copyright 2013-2018 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)

from lino.api import _
from lino_xl.lib.contacts.models import *
from lino_xl.lib.contacts.roles import ContactsStaff
from lino.modlib.users.choicelists import UserTypes


class PartnerDetail(PartnerDetail):

    main = "general accounting sepa.AccountsByPartner"

    general = dd.Panel("""
    address_box:60 contact_box:30 overview
    bottom_box
    """,
                       label=_("General"))

    accounting = dd.Panel("""
    payment_term purchase_account
    vat.VouchersByPartner
    accounting.MovementsByPartner
    """,
                      label=dd.plugins.accounting.verbose_name)

    address_box = dd.Panel("""
    name_box
    country region city zip_code:10
    addr1
    street_prefix street:25 street_no street_box
    addr2
    """,
                           label=_("Address"))

    contact_box = dd.Panel("""
    info_box
    email:40
    url
    phone
    gsm fax
    """,
                           label=_("Contact"))

    bottom_box = """
    remarks
    """

    name_box = "name"
    info_box = "id language"


class PersonDetail(PartnerDetail, PersonDetail):

    name_box = "last_name first_name:15 gender title:10"
    info_box = "id:5 language:10"
    bottom_box = "remarks contacts.RolesByPerson"


class CompanyDetail(PartnerDetail, CompanyDetail):
    main = "general accounting sepa.AccountsByPartner storage.FillersByPartner"

    bottom_box = """
    remarks contacts.RolesByCompany
    """

    name_box = "prefix:10 name type:30"


class MyCompany(Companies):
    label = _("My company")
    default_record_id = "row"

    @classmethod
    def get_row_permission(cls, obj, ar, state, ba):
        if obj == ar.get_user().ledger.company:
            return True
        # return super().get_row_permission(obj, ar, state, ba)
        return False

    @classmethod
    def get_row_by_pk(cls, ar, pk):
        return ar.get_user().ledger.company
