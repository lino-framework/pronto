from lino.api import _
from lino_xl.lib.contacts.fixtures.std import *

old_objects = objects


def objects():
    yield old_objects()

    RoleType = rt.models.contacts.RoleType

    yield RoleType(**dd.str2kw('name', _("Distributor")))
    yield RoleType(**dd.str2kw('name', _("POS Agent")))
    yield RoleType(**dd.str2kw('name', _("Supply staff")))
    yield rt.models.contacts.Company(name='Miscellaneous')
    # yield rt.models.contacts.Person(name='Miscellaneous')
