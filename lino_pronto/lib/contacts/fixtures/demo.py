# -*- coding: UTF-8 -*-
# Copyright 2024 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)

from lino.api import rt, _
from lino.utils.instantiator import Instantiator
from lino_xl.lib.ledgers.utils import prepare_company

def objects():
    company = Instantiator('contacts.Company', "name barcode_identity").build
    role = Instantiator('contacts.Role', "type person company").build

    RoleType = rt.models.contacts.RoleType
    Person = rt.models.contacts.Person
    Company = rt.models.contacts.Company
    User = rt.models.users.User
    ceo = RoleType.objects.get(name='CEO')
    pos_agent = RoleType.objects.get(name='POS Agent')
    supply_staff = RoleType.objects.get(name='Supply staff')

    def prep_company(company_name, bid, roletype=ceo, username=None, fname=None, lname=None):
        user, _ = User.objects.get_or_create(username=username or company_name)
        person, created = Person.objects.get_or_create(
            first_name=fname or user.first_name or company_name, last_name=lname or user.last_name)
        if created:
            yield person
        user.partner = person
        yield user
        yield (c := company(company_name, bid))
        yield role(roletype, person, c)
        yield prepare_company(c, user)

    yield prep_company("MSM", 1)
    yield prep_company("Prodip", 2)
    yield prep_company("One plus", 3)
    yield prep_company("Osaca", 4)
    yield prep_company("SK", 5)
    yield prep_company("Power plus", 6)
    yield prep_company("KRS Business Center", 7, username='russel')
    yield prep_company("General electronics BD", 8, username="cleopatra")

    abunomani = User.objects.get(username='abunomani')
    gretel = User.objects.get(username='gretel')

    yield (p_an := Person(first_name=abunomani.first_name, last_name=abunomani.last_name))
    yield (p_hg := Person(first_name=gretel.first_name, last_name=gretel.last_name))

    abunomani.partner = p_an
    yield abunomani
    gretel.partner = p_hg
    yield gretel

    make_subscription = rt.models.ledgers.SubscribeToLedger.make_subscription
    ar = rt.models.ledgers.Ledger.get_default_table().request()

    cleopatra = User.objects.get(username='cleopatra')
    krs = Company.objects.get(barcode_identity=7)
    ge = Company.objects.get(barcode_identity=8)

    make_subscription(dict(company=ge, user=abunomani, role=pos_agent, ledger=None), ar)
    make_subscription(dict(company=krs, user=cleopatra, role=supply_staff, ledger=None), ar)
    make_subscription(dict(company=krs, user=gretel, role=supply_staff, ledger=None), ar)
