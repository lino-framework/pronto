import datetime
from ..settings import *
# from .users_db import DBSETTINGS


class Site(Site):
    is_demo_site = True
    the_demo_date = datetime.date(2019, 1, 18)
    languages  = "en bn et de fr"
    default_ui = "lino_react.react"


SITE = Site(globals())

DEBUG = True

ALLOWED_HOSTS = ['*']

# DATABASES = DBSETTINGS
# DATABASE_ROUTERS = ['lino_pronto.utils.db_routes_and_settings.DBRouter']
