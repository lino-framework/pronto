from pathlib import Path
from lino_pronto.utils.db_routes_and_settings import SETTINGS

DBSETTINGS = SETTINGS.setup({'ENGINE': 'django.db.backends.sqlite3', 'NAME': str(Path(__file__).parent / 'default.db')})
DBSETTINGS.add_user_scope('russel')
DBSETTINGS.add_user_scope('cleopatra')
