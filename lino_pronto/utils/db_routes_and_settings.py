from pathlib import Path
from collections.abc import MutableMapping

# ===================================== initdb mod =====================================
#             call_command('makemigrations', interactive=False, verbosity=verbosity)
#             for u in settings.DATABASES:
#                 conf = settings.DATABASES[u]
#                 if conf['ENGINE'] == 'django.db.backends.sqlite3':
#                     if (name := conf['NAME']) != ':memory:' and os.path.isfile(name):
#                         os.remove(name)
#                     try:
#                         del connections[u]
#                     except:
#                         pass
#                     if u != using:
#                         print('9'*80)
#                         call_command('migrate', '--run-syncdb', database=u)
#                         print('9'*80)
#                         del connections[u]


class DBSettings(MutableMapping):
    def setup(self, default):
        self.default = default
        self.store = ['default']
        return self

    def __getitem__(self, key):
        assert key in self.store
        if key == 'default':
            return self.default
        return self._make_dup(key)

    def __setitem__(self, key, value):
        raise NotImplementedError

    def __delitem__(self, key):
        raise NotImplementedError

    def __len__(self):
        return len(self.store)

    def __iter__(self):
        return iter(self.store)

    def _make_dup(self, key):
        dbsetting = self.default.copy()
        if dbsetting['ENGINE'].endswith('sqlite3'):
            key = str(Path(dbsetting['NAME']).parent / f"{key}.db")
        dbsetting.update(NAME=key)
        return dbsetting

    def add_user_scope(self, username):
        self.store.append(username)


SETTINGS = s = DBSettings()

class DBRouter:
    def get_db_attr(self, instance):
        if instance is None:
            return None
        if hasattr(instance, 'using'):
            return instance.using
        return None
    def db_for_read(self, model, **hints):
        if (using := self.get_db_attr(hints.get('instance', None))) is not None:
            print('!'*80)
            print("write db", model, hints, using)
            return using
        return 'default'

    def db_for_write(self, model, **hints):
        if (using := self.get_db_attr(hints.get('instance', None))) is not None:
            print('!'*80)
            print("write db", model, hints, using)
            return using
        return 'default'

    def allow_relation(self, obj1, obj2, **hints):
        return True

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        if (model := hints.get('model', None)) is None:
            if model_name is None:
                raise Exception("Unknown edge case")
            else:
                from lino.api import dd
                model = dd.resolve_model(f"{app_label}.{model_name}")
                if isinstance(model, dd.UnresolvedModel):
                    if db == 'default':
                        return True
                    return False
        if not hasattr(model, 'sharedDB') or model.sharedDB:
            if db == 'default':
                return True
            return False
        else:
            if hasattr(model, 'sharedDB'):
                return True
            # if db != 'default':
            #     return True
            return False


# if __name__=='__main__':
#     DBSETTINGS = DBSettings({'ENGINE': 'django.db.backends.sqlite3', 'NAME': str(Path(__file__).parent / 'default.db')})
#     DBSETTINGS.add_user_scope('russel')
#     DBSETTINGS.add_user_scope('cleopatra')
#     print('='*80)
#     print(DBSETTINGS['russel'])
