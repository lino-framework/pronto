===========================
The ``lino-pronto`` package
===========================





**Lino Pronto** is a Lino application for assembling and selling products.

- Project homepage: https://gitlab.com/lino-framework/pronto

- Documentation: https://lino-framework.gitlab.io/pronto

- Commercial information: https://www.saffre-rumma.net


